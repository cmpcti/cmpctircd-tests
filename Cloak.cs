using System;
using System.Net;
using cmpctircd.Configuration;
using FakeItEasy;
using NUnit.Framework;

namespace cmpctircd.Tests
{
    public class CloakTests
    {
        private IServiceProvider _fakeServiceProvider;
        private CmpctConfigurationSection config;
        private IRCd ircd;
        private Log log;

        [SetUp]
        public void Setup()
        {
            _fakeServiceProvider = A.Fake<IServiceProvider>();
            config = CmpctConfigurationSection.GetConfiguration();
            log = new Log();
            ircd = new IRCd(log, config, _fakeServiceProvider);
        }

        [Test]
        public void TestCloakDomain()
        {
            var host = "test.cmpct.info";
            var cloak = Cloak.GetCloak(host, null, ircd.CloakKey, false);
            Assert.AreEqual("cmpct-dltnfc.cmpct.info", cloak);
        }

        [Test]
        public void TestCloakIPv4()
        {
            // TODO: Test with more IPs (non-loopback)
            var IP = IPAddress.Loopback;
            var cloak = Cloak.InspCloakIPv4(IP, ircd.CloakKey, true);
            Assert.AreEqual("cmpct-7t0.v64.q8dsol.IP", cloak);
        }

        [Test]
        public void TestCloakIPv6()
        {
            // TODO: Test with more IPs (non-loopback)
            var IP = IPAddress.IPv6Loopback;
            var cloak = Cloak.GetCloak(null, IP, ircd.CloakKey, true);
            Assert.AreEqual("cmpct-g7hb88.479u.kc7f.truk2h.IP", cloak);
        }
    }
}