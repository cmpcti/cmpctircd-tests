using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using cmpctircd.Configuration;
using cmpctircd.Threading;
using FakeItEasy;
using IrcDotNet;
using NUnit.Framework;

namespace cmpctircd.Tests
{
    public class ConnectTests
    {
        private IServiceProvider _fakeServiceProvider;
        private CmpctConfigurationSection config;

        private IrcClient irc;
        private IRCd ircd;
        private Log log;

        private Task task;

        [SetUp]
        public void Setup()
        {
            _fakeServiceProvider = A.Fake<IServiceProvider>();
            task = Task.Run(() =>
                {
                    using (var sc = new QueuedSynchronizationContext())
                    {
                        config = CmpctConfigurationSection.GetConfiguration();
                        SynchronizationContext.SetSynchronizationContext(sc);

                        log = new Log();
                        ircd = new IRCd(log, config, _fakeServiceProvider);

                        log.Initialise(ircd, config.Loggers.OfType<LoggerElement>().ToList());
                        ircd.Run();
                        sc.Run();
                    }
                }
            );

            // Hack to give the IRCd long enough to listen before we start trying to connect to it
            // TODO: Can we do without this?
            task.Wait(2000);
        }

        [TearDown]
        public void Teardown()
        {
            // TODO: Shut down the ircd/listener
            ircd.Stop();
        }

        [Test]
        public void TestBasicConnectAndRegistration()
        {
            var client = new StandardIrcClient();

            using (var connectedEvent = new ManualResetEventSlim(false))
            {
                var registrationInfo = new IrcUserRegistrationInfo
                {
                    NickName = "cmpct_test",
                    Password = "",
                    UserName = "cmpct_test",
                    RealName = "cmpct_test"
                };
                client.Connected += (sender, e) => connectedEvent.Set();
                client.Connect("127.0.0.1", 6667, false, registrationInfo);

                if (!connectedEvent.Wait(2000))
                {
                    client.Dispose();
                    Assert.Fail("ConnectedEvent not called: server not listening?");
                }

                using (var registrationEvent = new ManualResetEventSlim(false))
                {
                    client.Registered += (sender, e) => registrationEvent.Set();

                    if (!registrationEvent.Wait(2000))
                    {
                        client.Dispose();
                        Assert.Fail("RegistrationEvent not called: not registered?");
                    }
                }
            }
        }

        [Test]
        public void TestJoinChannel()
        {
            var client = new StandardIrcClient();
            var registrationInfo = new IrcUserRegistrationInfo
            {
                NickName = "cmpct_test",
                Password = "",
                UserName = "cmpct_test",
                RealName = "cmpct_test"
            };

            client.Connect("127.0.0.1", 6667, false, registrationInfo);


            using (var registrationEvent = new ManualResetEventSlim(false))
            {
                // Need to include the registration check for the delay
                client.Registered += (sender, e) => registrationEvent.Set();

                if (!registrationEvent.Wait(2000))
                {
                    client.Dispose();
                    Assert.Fail("RegistrationEvent not called: not registered?");
                }
            }

            using (var joinedChannelEvent = new ManualResetEventSlim(false))
            {
                client.LocalUser.JoinedChannel += (sender, e) => joinedChannelEvent.Set();
                client.Channels.Join("#test");

                if (!joinedChannelEvent.Wait(2000))
                {
                    client.Dispose();
                    Assert.Fail("JoinedChannelEvent not called: channel not joined?");
                    if(!joinedChannelEvent.Wait(2000)) {
                        Quit(client);
                        Assert.Fail("JoinedChannelEvent not called: channel not joined?");
                    }

                    Quit(client);
                }
            }
        }

        public void Quit(StandardIrcClient client) {
            // Possibly related to https://github.com/IrcDotNet/IrcDotNet/issues/54
            client.SendRawMessage("QUIT :Test client leaving\r\n");
            client.Quit();
            client.Disconnect();
            client.Dispose();
        }
    }
}
